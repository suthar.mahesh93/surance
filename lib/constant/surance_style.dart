import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

final TextStyle fontStyleNormal = GoogleFonts.workSans(
    fontSize: 18, fontWeight: FontWeight.w400, color: Color(0xff040415));

final TextStyle fontStyleMedium = GoogleFonts.workSans(
    fontSize: 18, fontWeight: FontWeight.w500, color: Color(0xff040415));

final TextStyle fontStyleSemiBold = GoogleFonts.workSans(
    fontSize: 18, fontWeight: FontWeight.w600, color: Color(0xff040415));
