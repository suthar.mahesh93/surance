import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:surance/constant/surance_style.dart';
import 'package:webview_flutter/webview_flutter.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: Color(0xFF00529C),
        title: Text(
          "Surance Consultancy",
          style: fontStyleMedium.copyWith(color: Colors.white, fontSize: 20),
        ),
        elevation: 3,
        automaticallyImplyLeading: false,
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: "https://surance.co.in/life-insurance/",
            javascriptMode: JavascriptMode.unrestricted,
            onPageStarted: (String url) {
              print('Page started loading: $url');
              setState(() {
                isLoading = true;
              });
            },
            onPageFinished: (String url) {
              setState(() {
                isLoading = false;
              });
            },
          ),
          Center(
            child: Visibility(
              child: CircularProgressIndicator(
                color: Color(0xFF00529C),
              ),
              visible: isLoading,
            ),
          )
        ],
      ),
    );
  }
}
