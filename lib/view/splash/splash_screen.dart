import 'dart:async';

import 'package:flutter/material.dart';
import 'package:surance/constant/surance_style.dart';
import 'package:surance/view/home/home_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() {
    super.dispose();
  }

  init() async {
    Timer(
        Duration(seconds: 3),
        () => {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => HomeScreen()))
            });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: const Color(0xFF00529C),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/surance_logo.png',
                  height: 80,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5, right: 5),
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Developed by ",
                  style: fontStyleNormal.copyWith(
                      fontSize: 12, color: Colors.white),
                ),
                Text(
                  "Kreative TechNest",
                  style: fontStyleMedium.copyWith(
                      fontSize: 12, color: Colors.white),
                ),
              ],
            ),
          )
        ],
      ),
    ));
  }
}
