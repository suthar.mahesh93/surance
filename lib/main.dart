import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:surance/view/splash/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(Surance());
}

class Surance extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    return MaterialApp(
      title: "Surance",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/splash': (context) => SplashScreen(),
      },
    );
  }
}
